#!/bin/bash

##########################################
# Script de construction d'une PrimTux 6 #
# sur Raspberry Pi disposant d'une       #
# Raspberry Pi OS lite installée.        #
# Auteur: Philippe Ronflette             #
# philippe.dpt35@yahoo.fr                #
##########################################

debut=$(date +%s)

# Paramétrage des sources, à modifier si nécessaire
sources="https://framagit.org/Steph/primtux6/-/raw/master/armhf/sources/includes.chroot.tar.gz https://framagit.org/Steph/primtux6/-/raw/master/armhf/sources/hooks.tar.gz"
#sources="https://framagit.org/Steph/primtux6/-/raw/master/ptx6-Ubuntu20.04-amd64/includes.chroot.tar.gz https://framagit.org/Steph/primtux6/-/raw/master/armhf/sources/hooks.tar.gz"
sources_jlodb="https://framagit.org/Steph/primtux6/-/raw/master/ptx6-Debian10-i386/config/packages/"
archive1="includes.chroot.tar.gz"
archive2="hooks.tar.gz"
config_pcmanfm="https://www.primtux.fr/Documentation/armhf/pcmanfm.conf.tar"
config_libfm="https://www.primtux.fr/Documentation/armhf/libfm.conf.tar"
config_roxterm="https://www.primtux.fr/Documentation/armhf/Profiles.tar"
icone_whisker="https://www.primtux.fr/Documentation/armhf/ardoise-primtux.png"

version="PrimTux6 Debian10 RPi"

# Listes des paquets à installer
# Les paquets network-manager network-manager-gnome ont été éliminés de la liste en raison d'éventuels conflits avec ctparental. Rééxaminer leur situation
# Les firmware atmel-firmware dahdi-firmware-nonfree firmware-adi ffirmware-myricom irmware-ipw2x00 firmware-ivtv firmware-iwlwifi firmware-amd-graphics firmware-bnx2 firmware-bnx2x firmware-ath9k-htc firmware-ath9k-htc-dbgsym firmware-cavium firmware-intel-sound firmware-intelwimax firmware-linux firmware-linux-free firmware-linux-nonfree firmware-netronome firmware-netxen firmware-qcom-media firmware-qlogic firmware-ralink firmware-siano firmware-ti-connectivity hdmi2usb-fx2-firmware firmware-zd1211 grub-common grub2-common xserver-xorg-video-radeon xserver-xorg-video-amdgpu xserver-xorg-video-ati ont également été enlevés de la liste
paquets_base="accountsservice acl acpid adduser adwaita-icon-theme anacron ant apparmor aptitude aptitude-common arandr aria2 aspell aspell-fr at-spi2-core audacity avahi-autoipd baobab blt breeze-icon-theme ca-certificates-java catfish cdparanoia cpulimit cups-client cups-common cups-pk-helper dbus-user-session dbus-x11 dconf-cli dconf-service debian-reference-common default-jre default-jre-headless desktop-base desktop-file-utils dictionaries-common disk-manager dns323-firmware-tools eject enchant evince exfat-fuse exfat-utils expeyes-firmware-dev faenza-icon-theme fbterm ffmpeg ffmpegthumbnailer file-roller filezilla filezilla-common firefox-esr firefox-esr-l10n-fr firmware-microbit-micropython firmware-microbit-micropython-doc firmware-samsung flac fluxbox font-manager fontconfig fontconfig-config fonts-dejavu fonts-dejavu-core fonts-dejavu-extra fonts-droid-fallback fonts-freefont-ttf fonts-liberation fonts-liberation2 fonts-opendyslexic fonts-opensymbol fonts-sil-andika fonts-sil-gentium fonts-sil-gentium-basic fonts-wine fotowall frei0r-plugins fskbsetting fxload galternatives gdebi gdebi-core geany geany-common genisoimage geoclue-2.0 gigolo gnome-calculator gnome-desktop3-data gnome-font-viewer gnome-getting-started-docs gnome-icon-theme gnome-icon-theme-symbolic gnome-keyring gnome-screenshot gnome-system-tools gparted gpicview gsettings-desktop-schemas gsfonts gsfonts-x11 gtk-update-icon-cache gtk2-engines gucharmap gvfs-backends gvfs-common gvfs-daemons gvfs-fuse hardinfo haveged hicolor-icon-theme hpijs-ppds hplip hplip-data hplip-gui hunspell-fr hunspell-fr-classical iio-sensor-proxy im-config imagemagick itools java-common java-wrappers javascript-common kde-l10n-fr kded5 kdenlive kdenlive-data kio konwert konwert-filters lame libgconf-2-4 libgtk3-perl libreoffice libreoffice-gtk3 libreoffice-help-fr libreoffice-l10n-fr libreoffice-report-builder libreoffice-style-tango libsnack-alsa libtk-img lightning lintian live-tools lp-solve lsof lv lxappearance lxpanel marble media-player-info menu menulibre midisport-firmware mousepad mpv murrine-themes mysql-common mythes-fr network-manager network-manager-gnome nginx notification-daemon numlockx nxt-firmware ofono onboard openboard onboard-common os-prober osmo oxygen-icon-theme p7zip p7zip-full pavucontrol pcmanfm pdfsam photoflare plasma-framework pulseaudio pulseaudio-utils python-glade2 python-gtk2 python-tk python-tk rox-filer roxterm samba samba-common samba-common-bin sane-utils scribus scribus-data sddm sigrok-firmware-fx2lafw smplayer smplayer-l10n smplayer-themes smtube sonnet-plugins sound-icons soundconverter spacefm speech-dispatcher speech-dispatcher-espeak-ng synaptic system-config-printer system-config-printer-common system-config-samba task-french task-french-desktop task-laptop tcl tcl8.6 tcl-snack time tix tk tk8.6 tk8.6-blt2.5 tk8.6-blt2.5 trash-cli ttf-aenigma ttf-dejavu ttf-dejavu-core ttf-dejavu-extra ttf-unifont update-inetd upower ure usbmuxd user-setup util-linux-locales uuid-runtime wfrench wine winff winff-data winff-gtk2 x11-apps x11-common x11-session-utils x11-utils x11-xkb-utils x11-xserver-utils xbitmaps xbrlapi xdg-dbus-proxy xdg-utils xfburn xfce4-cpufreq-plugin xfce4-datetime-plugin xfce4-fsguard-plugin xfce4-indicator-plugin xfce4-netload-plugin xfce4-notifyd xfce4-panel xfce4-pulseaudio-plugin xfce4-systemload-plugin xfce4-whiskermenu-plugin xfconf xfonts-100dpi xfonts-75dpi xfonts-base xfonts-encodings xfonts-scalable xfonts-unifont xfonts-utils xinit xorg xorg-docs-core xournalpp xsane xsane-common xscreensaver xscreensaver-data xserver-common xserver-xorg xserver-xorg-core xserver-xorg-input-all xserver-xorg-input-libinput xserver-xorg-input-wacom xserver-xorg-legacy xserver-xorg-video-all xserver-xorg-video-fbdev xserver-xorg-video-nouveau xserver-xorg-video-qxl xserver-xorg-video-vesa xsettingsd xterm xxkb yelp yelp-xsl youtube-dl zenity zenity-common zip"

paquets_educatifs="abuledu-aller-primtux abuledu-associations-primtux abuledu-microtexte abuledu-minitexte abuledu-puzzle achats alacampagne association-images association-images2 aujardin avoir-etre balance-virtuelle blocs-logiques chiffres-lettres croissant-decroissant eduactiv8 gcompris-qt geotortue-stretch goldendict gtans histoires jclic jclicpuzzle-primtux kgeography klettres klettres-data ktuberling kturtle leterrier-calculment leterrier-chronosphere leterrier-cibler leterrier-contour leterrier-fubuki leterrier-imageo leterrier-mulot leterrier-suitearithmetique leterrier-tierce leximots lis-ecris lis-ecris2 microscope-virtual-primtux multiplication-station-primtux musescore omnitux-light ordre-alphabetique pendu-peda-gtk pysycache pysycache-buttons-beerabbit pysycache-buttons-crapaud pysycache-buttons-ice pysycache-buttons-wolf pysycache-click-dinosaurs pysycache-click-sea pysycache-dblclick-appleandpear pysycache-dblclick-butterfly pysycache-i18n pysycache-images pysycache-move-animals pysycache-move-food pysycache-move-plants pysycache-move-sky pysycache-move-sports pysycache-puzzle-cartoons pysycache-puzzle-photos pysycache-sounds qdictionnaire ri-li ri-li-data scratch stellarium stellarium-data tbo tuxmath tuxpaint tuxpaint-config toutenclic "

paquets_primtux="accueil-primtux6 administration-eleves-primtux arreter-primtux fluxboxlauncher gtkdialog handymenu mothsart-wallpapers-primtux"

paquets_rpi="sauve-carte yad"

paquets_jlodb="poufpoufce1_19.10.0_all.deb poufpoufce2_19.10.0_all.deb poufpoufcm2_19.10.0_all.deb poufpoufcp_19.10.0_all.deb poufpoufinfo_19.10.0_all.deb poufpoufjeux_19.10.0_all.deb poufpoufxyz_19.10.0_all.deb"

# Redirection d'erreurs vers un fichier log
fichierlog="/var/log/install-primtux-rpi.log"
if ! [ -e "$fichierlog" ]
    then > "$fichierlog"
fi
echo "###############################################################################################
Lancement du script de construction" >> "$fichierlog"
date >> "$fichierlog"
exec 2>>"$fichierlog"

# Téléchargement des sources de configuration de PrimTux 6
debut_telechargement=$(date +%s)
mkdir /tmp/sources-primtux
echo "Téléchargement des sources. Cela peut demander du temps."
wget -P /tmp/sources-primtux $sources 2>&1 | tee -a "$fichierlog"
wget -P /tmp "$config_pcmanfm" 2>&1 | tee -a "$fichierlog"
wget -P /tmp "$config_libfm" 2>&1 | tee -a "$fichierlog"
wget -P /tmp "$config_roxterm" 2>&1 | tee -a "$fichierlog"
if ! [ -e /tmp/sources-primtux/"$archive1" ] || ! [ -e /tmp/sources-primtux/"$archive2" ]; then
  echo "Des fichiers archives essentiels n'ont pu être téléchargés.
Le script va s'arrêter.
Veuillez vérifier votre connexion Internet et relancer le script." | tee -a "$fichierlog"
  exit 1
fi
fin_telechargement=$(date +%s)
temps_telechargement=$(($fin_telechargement-$debut_telechargement))
temps_telechargement=$(echo $temps_telechargement |awk '{printf "%02d:%02d:%02d\n",$1/3600,$1%3600/60,$1%60}')
echo "Fin de téléchargement des fichiers sources
------------------------------------------" | tee -a "$fichierlog"

# Ajout des dépôts PrimTux
echo "deb https://depot.primtux.fr/repo/debs/ PrimTux6-armhf main" > /etc/apt/sources.list.d/primtux6.list
wget -O - https://depot.primtux.fr/repo/debs/key/PrimTux.gpg.key | apt-key add -

# Ajout du dépôt non libre
echo "deb https://deb.debian.org/debian buster non-free" >> /etc/apt/sources.list
wget -q https://ftp-master.debian.org/keys/release-10.asc -O- | apt-key add -

# Téléchargement des paquets libttspico
wget -N https://ftp.debian.org/debian/pool/non-free/s/svox/libttspico-utils_1.0+git20130326-9_armhf.deb
wget -N https://ftp.debian.org/debian/pool/non-free/s/svox/libttspico0_1.0+git20130326-9_armhf.deb

# Ajout du dépôt log2ram
echo "deb https://packages.azlux.fr/debian/ buster main" | sudo tee /etc/apt/sources.list.d/azlux.list
wget -qO - https://azlux.fr/repo.gpg.key | apt-key add -

# Installation des paquets
debut_paquets=$(date +%s)
apt-get update
apt-get dist-upgrade -y
echo "Fin de mise à jour du système." | tee -a "$fichierlog"

paquets_introuvables=""

for paquet in ${paquets_base}
do
	existe=$(apt-cache show "$paquet")
	if [ -n "$existe" ]; then
	  if [ "$paquet" = "samba" ]; then
        temps_pause_on=$(date +%s)
	    apt-get install -y "$paquet"
		temps_pause_off=$(date +%s)
	  else 
	  	apt-get install -y "$paquet"
	  fi
	else 
	  paquets_introuvables="$paquets_introuvables $paquet"
	fi
done
echo "Fin d'installation des paquets de base
--------------------------------------" | tee -a "$fichierlog"

# Installation des paquets libttspico
apt-get install -y libttspico-data
dpkg -i libttspico0_1.0+git20130326-9_armhf.deb
dpkg -i libttspico-utils_1.0+git20130326-9_armhf.deb

for paquet in ${paquets_educatifs}
do
	existe=$(apt-cache show "$paquet")
	if [ -n "$existe" ]; then
	  apt-get install -y "$paquet"
	else 
	  paquets_introuvables="$paquets_introuvables $paquet"
	fi
done
echo "Fin d'installation des Paquets éducatifs
----------------------------------------" | tee -a "$fichierlog"

for paquet in ${paquets_primtux}
do
	existe=$(apt-cache show "$paquet")
	if [ -n "$existe" ]; then
	  apt-get install -y "$paquet"
	else 
	  paquets_introuvables="$paquets_introuvables $paquet"
	fi
done
echo "Fin d'installation des paquets PrimTux
--------------------------------------" | tee -a "$fichierlog"

for paquet in ${paquets_rpi}
do
	existe=$(apt-cache show "$paquet")
	if [ -n "$existe" ]; then
	  apt-get install -y "$paquet"
	else 
	  paquets_introuvables="$paquets_introuvables $paquet"
	fi
done
echo "Fin d'installation des paquets RPi
----------------------------------" | tee -a "$fichierlog"

# Installation des applications jLoDB poufpouf
mkdir /tmp/sources-primtux/jlodb
for fichier in ${paquets_jlodb}; do
   wget -N -P /tmp/sources-primtux/jlodb "$sources_jlodb$fichier"
   dpkg -i "/tmp/sources-primtux/jlodb/$fichier"
done

apt-get install log2ram

apt-get --fix-broken install -y
fin_paquets=$(date +%s)

temps_pause=$(($temps_pause_off-$temps_pause_on))
temps_paquets=$(($fin_paquets-$debut_paquets))
temps_paquets=$(($temps_paquets-$temps_pause))
temps_pause=$(echo $temps_pause |awk '{printf "%02d:%02d:%02d\n",$1/3600,$1%3600/60,$1%60}')
temps_paquets=$(echo $temps_paquets |awk '{printf "%02d:%02d:%02d\n",$1/3600,$1%3600/60,$1%60}')
echo "Fin des opérations d'installation des paquets en $temps_paquets" | tee -a "$fichierlog"

# Changement de gestionnaire réseaux
apt-get remove wicd wicd-daemon
if ! grep "denyinterfaces wlan0" /etc/dhcpcd.conf; then
  echo "denyinterfaces wlan0" >>/etc/dhcpcd.conf
fi
if ! grep dhcp=internal /etc/NetworkManager/NetworkManager.conf; then
  sed -i '/plugins=ifupdown,keyfile/ adhcp=internal' /etc/NetworkManager/NetworkManager.conf
fi
sed -i 's/managed=false/managed=true/' /etc/NetworkManager/NetworkManager.conf

if [ -n "$paquets_introuvables" ]; then
   echo "Paquets non disponibles dans les dépôts :" >>"$fichierlog"
   echo "$paquets_introuvables" >>"$fichierlog"
fi

# Configuration de PrimTux 6
mkdir /tmp/sources-primtux/hooks
tar --keep-directory-symlink --overwrite -xzvf /tmp/sources-primtux/"$archive1" -C /
tar xzvf /tmp/sources-primtux/"$archive2" -C /tmp/sources-primtux/hooks
rm /tmp/sources-primtux/"$archive1" /tmp/sources-primtux/"$archive2"

# Remplace les fichiers de configuration du gestionnaire de fichiers par ceux pour RPi
tar xvf /tmp/$(basename "$config_pcmanfm") -C /etc/skel/.config/pcmanfm/default
tar xvf /tmp/$(basename "$config_libfm") -C /etc/skel/.config/libfm
# Configuration de roxterm
tar xvf /tmp/$(basename "$config_roxterm") -C /etc/skel/.config/roxterm.sourceforge.net
rm /tmp/$(basename "$config_pcmanfm")
rm /tmp/$(basename "$config_libfm")
rm /tmp/$(basename "$config_roxterm")

# Suppression de l'utilisateur pi
userdel -r pi

# Modification de la configuration du RPi pour sddm
sed -i 's/^dtoverlay/#dtoverlay/' /boot/config.txt
sed -i '/\[all\]/ adtoverlay=vc4-fkms-v3d' /boot/config.txt

# Application des scripts de configuration de PrimTux
sh /tmp/sources-primtux/hooks/normal/1000.useradd.hook.chroot
# Copie les répertoires de chaque utilisateur
rsync -av /etc/skel/ /home/administrateur
rsync -av /etc/skel-mini/ /home/01-mini
rsync -av /etc/skel-super/ /home/02-super
rsync -av /etc/skel-maxi/ /home/03-maxi

# on donne à chaque répertoire utilisateur ses droits
chown -R 01-mini:01-mini /home/01-mini
chown -R 02-super:02-super /home/02-super
chown -R 03-maxi:03-maxi /home/03-maxi
chown -R administrateur:administrateur /home/administrateur
chmod -R 2777 /home/administrateur/Public
# Change les shells utilisateurs
chsh -s /bin/bash administrateur;
chsh -s /bin/false 01-mini;
chsh -s /bin/false 02-super;
chsh -s /bin/false 03-maxi;

# Nettoyage des dossiers .wine inutiles sur RPi
rm -rf /home/{01-mini,02-super,03-maxi,administrateur}/.wine

# On nettoie !
rm -rf /tmp/sources-primtux
rm -rf tmp
rm libttspico0_1.0+git20130326-9_armhf.deb
rm libttspico-utils_1.0+git20130326-9_armhf.deb

# Icône pour le menu whisker
wget "$icone_whisker"
mv $(basename "$icone_whisker") /usr/share/pixmaps

# Suppression des lanceurs inutiles
rm /usr/share/applications/wine.desktop
rm -rf /usr/share/applications/screensavers

# Vérification des paquets installés
paquets_jlodb=$(echo "$paquets_jlodb" | sed 's/_19.10.0_all.deb//g')
paquets=$(echo "$paquets_base $paquets_educatifs $paquets_primtux $paquets_rpi $paquets_jlodb")
dpkg -l | sed -e '1,6d' -e "s/[ ][ ]*/#/g" | cut -d '#' -f 2 > /tmp/installes.txt
echo "Paquets manquants :" >> "$fichierlog"
for paquet in ${paquets}
do
   if ! grep "$paquet" /tmp/installes.txt > /dev/null; then
      echo "$paquet" >> "$fichierlog"
   fi
done
rm /tmp/installes.txt

# Indication de version de l'OS
echo "$version" >/etc/primtux_version

# Nettoyage du fichier de suivi des opérations
sed -i '/[1-9].*K \.\.\.\.*/ d' "$fichierlog"
fin=$(date +%s)

# Calul des temps de construction
temps_total=$(($fin-$debut))
temps_total=$(($temps_total-$temps_pause))
temps_total=$(echo $temps_total |awk '{printf "%02d:%02d:%02d\n",$1/3600,$1%3600/60,$1%60}')

echo "Les opérations sont terminées et ont duré au total $temps_total dont :
  - $temps_telechargement en temps de téléchargement des fichiers de configuration ;
  - $temps_paquets en temps de téléchargement et d'installation de paquets." | tee -a "$fichierlog"
echo "
Fin des opérations" >> "$fichierlog"
echo "Un fichier du déroulement des opérations et des erreurs a été créé en $fichierlog
Veuillez redémarrer le système." 

exit 0
